# README #

Tool to import pingfed logs into a database

### Connection

To configure create an application.properties file with teh below structure

````
database.connectionurl=jdbc:mysql://<host>:3306/<database name>>
database.username=<username>>
database.password=<password>
database.size=200
````
You adjust the size of the batch (number of records to commit at once) using the database.size parameter 

### Run against yesterdays PingFederate audit logs

YESTERDAY=$(date -d "yesterday 13:00" '+%Y-%m-%d')

java -jar ping-log-to-db-1.0-SNAPSHOT.jar --type=pf-audit --log=/<path>/audit.${YESTERDAY}.log