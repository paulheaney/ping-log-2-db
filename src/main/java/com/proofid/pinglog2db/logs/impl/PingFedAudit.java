/*
 * Application: Ping Log 2 DB importer
 *      Author: Paul Heaney <paul.heaney@proofid.com>
 *     Company: ProofID Ltd.
 *       Legal: Copyright (C) 2020 ProofID Ltd. All Rights Reserved.
 *
 */
package com.proofid.pinglog2db.logs.impl;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component
public class PingFedAudit  {

    private String INSERT_SQL = "INSERT INTO audit_log (dtime, event,username,ip,app,host,protocol,role,partnerid,status,adapterid,description,responsetime, trackingid) " +
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private PreparedStatement preparedStatement;

    private static final String DELIMITER = "|";

    private Connection conn;

    @Autowired
    Environment env;

    @Value("${database.connectionurl}")
    protected String connectionurl;

    @Value("${database.username}")
    protected String user;

    @Value("${database.password}")
    protected String password;

    @Value("${database.size}")
    protected int size;

    private static final String BZ2 = "bz2";
    private static final String GZ = "gz";

    protected Logger log = LoggerFactory.getLogger(getClass());

    public PingFedAudit() throws SQLException {
        super();
    }

    @PostConstruct
    public void postConstruct() {
        log.info("Post construct");
        log.info("User: " + user);
        if (this.conn == null) {
            try {
                conn = DriverManager.getConnection(connectionurl, user, password);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    protected Connection getConnection() throws SQLException {
        if (this.conn == null) {
            log.info("User: " + user);
            conn = DriverManager.getConnection(connectionurl, user, password);
        }

        return this.conn;
    }

    protected BufferedReader readFileLineByLine(String file, String compression) throws FileNotFoundException {
        if (compression == null) {
            return new BufferedReader(new FileReader(file));
        } else if (compression.equals(BZ2) || compression.equals(GZ)) {
            FileInputStream fin = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fin);

            try {
                CompressorInputStream input  = new CompressorStreamFactory().createCompressorInputStream(bis);
                return new BufferedReader(new InputStreamReader(input));
            } catch (CompressorException e) {
                log.error("Unhandled compression type", e);
            }

        }

        throw new RuntimeException("Unsupported compression algorithm '" + compression + "'");
    }


    // @Override
    public void toDb(String logFilePath, String compression) {

        try {

            if (preparedStatement == null) {
                preparedStatement = getConnection().prepareStatement(INSERT_SQL);
            }

            BufferedReader reader = readFileLineByLine(logFilePath, compression);

            String line = reader.readLine();

            int count = 0;

            int success = 0;
            int fails = 0;

            while (line != null) {

                if (lineToDb(line)) {
                    success = success + 1;
                } else {
                    fails = fails + 1;
                }

                if (count % size == 0) {
                    preparedStatement.executeBatch();
                }

                count++;
                line = reader.readLine();
            }

            preparedStatement.executeBatch();

            log.info("Imported {} successfully and {} failed", success, fails);

        } catch (IOException ioe) {
            log.error("Error reading log", ioe);
        } catch (SQLException sqle) {
            log.error("Error writing to database", sqle);
        }
    }

    private boolean lineToDb(String line) throws SQLException {

        if (StringUtils.isNotEmpty(line)) {
            // 2020-10-18 00:00:33,231| tid:3BwXJFMdSb0ZMBz5dJEyix8zcPU| AUTHN_ATTEMPT| | 10.153.41.26 | | user-management| | ip-10-153-40-22.us-west-2.compute.internal| IdP| inprogress| BentleyIDFirst| | 468
            // dtime, event,username,ip,app,host,protocol,role,partnerid,status,adapterid,description,responsetime, trackingid
            String[] fields = StringUtils.split(line, DELIMITER);

            if (fields.length >= 12) {
                String dtime = fields[0].trim();
                if (dtime.indexOf(",") > 0) {
                    dtime = dtime.substring(0, dtime.indexOf(","));
                }

                preparedStatement.setString(1, dtime);
                preparedStatement.setString(2, fields[2].trim());
                preparedStatement.setString(3, fields[3].trim());
                preparedStatement.setString(4, fields[4].trim());
                preparedStatement.setString(5, fields[5].trim());
                preparedStatement.setString(6, fields[6].trim());
                preparedStatement.setString(7, fields[7].trim());
                preparedStatement.setString(8, fields[8].trim());
                preparedStatement.setString(9, fields[9].trim());
                preparedStatement.setString(10, fields[10].trim());
                preparedStatement.setString(11, fields[11].trim());
                preparedStatement.setString(12, fields[12].trim());
                preparedStatement.setInt(13, Integer.parseInt(fields[13].trim()));
                preparedStatement.setString(14, fields[1].trim());
                preparedStatement.addBatch();

                return true;
            } else {
                log.error("Expecting 12 fields got {}, line was '{}'", fields.length, line);
            }
        }

        return false;
    }
}
