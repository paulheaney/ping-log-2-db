/*
 * Application: Ping Log 2 DB importer
 *      Author: Paul Heaney <paul.heaney@proofid.com>
 *     Company: ProofID Ltd.
 *       Legal: Copyright (C) 2020 ProofID Ltd. All Rights Reserved.
 *
 */
package com.proofid.pinglog2db;

import com.proofid.pinglog2db.logs.Log;
import com.proofid.pinglog2db.logs.impl.PingFedAudit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PingLog2DB implements CommandLineRunner {

    private static Logger LOG = LoggerFactory.getLogger(PingLog2DB.class);

    @Autowired
    private PingFedAudit pfAudit;

    public static void main(String[] args) {
        SpringApplication.run(PingLog2DB.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        run(new DefaultApplicationArguments(args));
    }

    public void run(ApplicationArguments args) throws Exception {
        if (args.getOptionValues("type") != null) {
            String type = args.getOptionValues("type").get(0);

            if (args.getOptionValues("log") != null) {
                String logFile = args.getOptionValues("log").get(0);


                String compression = null;
                if (args.getOptionValues("compression") != null) {
                    switch (args.getOptionValues("log").get(0)) {
                        case "bz2":
                        case "gz":
                            compression = args.getOptionValues("log").get(0);
                            break;
                    }
                }

                switch (type) {
                    case "pf-audit":
                        pfAudit.toDb(logFile, compression);
                        break;
                    default:
                        usage();
                        return;
                }
            } else {
                usage();
            }
        } else {
            usage();
        }
    }


    private void usage() {
        System.out.println("Usage: <jar> --type=pf-audit --log=<path to log> [--compression=gz|bz2]");
    }

}
