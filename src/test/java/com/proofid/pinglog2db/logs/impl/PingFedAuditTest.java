/*
 * Application: Ping Log 2 DB importer
 *      Author: Paul Heaney <paul.heaney@proofid.com>
 *     Company: ProofID Ltd.
 *       Legal: Copyright (C) 2020 ProofID Ltd. All Rights Reserved.
 *
 */
package com.proofid.pinglog2db.logs.impl;

import com.proofid.pinglog2db.AppProperties;
import org.junit.jupiter.api.Test;

public class PingFedAuditTest {

    @Test
    public void test() throws Exception {
        String log = "/Users/pheaney/lab/pingfed/log/audit.log";
        AppProperties appProperties = new AppProperties();
        appProperties.connectionurl = "jdbc:mysql://localhost:3306/dbimport";
        appProperties.username="root";
        appProperties.password="novell";
        appProperties.size=200;

        PingFedAudit pingFedAudit = new PingFedAudit(

        );
        pingFedAudit.toDb(log, null);

    }

    @Test
    public void testBz2() throws Exception {
        String log = "/Users/pheaney/tmp/audit.log.bz2";
        AppProperties appProperties = new AppProperties();
        appProperties.connectionurl = "jdbc:mysql://localhost:3306/dbimport";
        appProperties.username="root";
        appProperties.password="novell";
        appProperties.size=200;

        PingFedAudit pingFedAudit = new PingFedAudit(

        );
        pingFedAudit.connectionurl = appProperties.connectionurl;
        pingFedAudit.user = appProperties.username;
        pingFedAudit.password = appProperties.password;
        pingFedAudit.size = 200;

        pingFedAudit.toDb(log, "bz2");

    }

}
