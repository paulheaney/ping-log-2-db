/*
 * Application: Ping Log 2 DB importer
 *      Author: Paul Heaney <paul.heaney@proofid.com>
 *     Company: ProofID Ltd.
 *       Legal: Copyright (C) 2020 ProofID Ltd. All Rights Reserved.
 *
 */
package com.proofid.pinglog2db.logs;

import com.proofid.pinglog2db.AppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@Component
public abstract class Log {

    private Connection conn;

    @Value("${database.connectionurl}")
    protected String connectionurl;

    @Value("${database.username}")
    protected String user;

    @Value("${database.password}")
    protected String password;

    @Autowired
    Environment env;

    protected Logger log = LoggerFactory.getLogger(getClass());

    public Log() throws SQLException {


    }

    @PostConstruct
    public void postConstruct() {
        log.info("Post construct");
        log.info("User: " + user);
        if (this.conn == null) {
            log.info("User: " + user);
            if (env != null) {
                log.info("User2: " + env.getProperty("database.username"));
            }
            try {
                conn = DriverManager.getConnection(connectionurl, user, password);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    protected Connection getConnection() throws SQLException {
        if (this.conn == null) {
            log.info("User: " + user);
            if (env != null) {
                log.info("User2: " + env.getProperty("database.username"));
            }
            conn = DriverManager.getConnection(connectionurl, user, password);
        }

        return this.conn;
    }

    protected BufferedReader readFileLineByLine(String file) throws FileNotFoundException {
        return new BufferedReader(new FileReader(file));
    }

    public abstract void toDb(String logFilePath);
}
