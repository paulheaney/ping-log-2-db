/*
 * Application: Ping Log 2 DB importer
 *      Author: Paul Heaney <paul.heaney@proofid.com>
 *     Company: ProofID Ltd.
 *       Legal: Copyright (C) 2020 ProofID Ltd. All Rights Reserved.
 *
 */
package com.proofid.pinglog2db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@ConfigurationProperties("database")
public class AppProperties {

    public String connectionurl;

    public String username;

    public String password;

    public int size;

}
